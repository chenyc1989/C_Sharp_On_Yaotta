﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weights
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int hight = Convert.ToInt32(textBox1.Text);
            if (radioButton1.Checked == true)
            {
                label2.Text = (hight * 2.8).ToString();
            }
            else if (radioButton2.Checked == true)
            {
                label2.Text = (hight * 1.8).ToString();
            }
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            int hight = Convert.ToInt32(textBox1.Text);
            if (radioButton1.Checked == true)
            {
                label2.Text = (hight * 2.8).ToString() + Global.test;
            }
            else
            {
                label2.Text = (hight * 1.8).ToString();
            }
        }
    }
}
