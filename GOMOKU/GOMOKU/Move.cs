﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GOMOKU
{ 
    class Move
    {
        private static readonly int START = 75, R = 75;

        public bool CanBeInput(int x, int y)
        {
            int quotient_x, quotient_y;
            quotient_x = (x - START) % R;
            quotient_y = (y - START) % R;
            if (Math.Abs(quotient_x) < R & Math.Abs(quotient_y) < R)
                return true;
            else
                return false;
        }
    }
}