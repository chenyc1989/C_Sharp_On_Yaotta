﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GOMOKU
{
    public partial class Form1 : Form
    {
        private bool isBlock = true;
        public Form1()
        {
            InitializeComponent();
            //this.Controls.Add(new Piece(true, 10, 10));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("hi this is Mouse Click");
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            this.Controls.Add(new Piece(isBlock, e.X, e.Y));
            bool chk = new Move();

            label1.Text = "" + e.X + "," + e.Y;
            isBlock = !isBlock;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            //MessageBox.Show("" + Convert.ToByte(e.KeyCode));
            //ESC = 27
            if (e.KeyCode == Keys.Escape)
                //MessageBox.Show("即將離開，確定嗎?");
                Application.Exit();
        }
    }
}
