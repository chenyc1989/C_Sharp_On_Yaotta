﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Run
{
    public partial class Form1 : Form
    {
        int no = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = imageList1.Images[1];
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (no == 0)
            {
                pictureBox1.Image = imageList1.Images[1];
                no = 1;
                
            }
            else
            {
                pictureBox1.Image = imageList1.Images[0];
                no = 0;
            }
            label1.Text = "Now is " + no;
        }
    }
}
