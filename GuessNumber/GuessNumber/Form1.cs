﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuessNumber
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private static readonly int ANS = new Random().Next(100);
        private  int MIN=1, MAX=100;
        private static string HINT;
        

        //MessageBox.show("");
        private bool chkInputIsDigits()
        {
            bool isDigits = int.TryParse(textBox1.Text, out int res);
            if (isDigits)
            {
                if (res > MAX || res < MIN)
                {
                    MessageBox.Show($"Plz Input {MIN}~{MAX}!!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            else
            {
                MessageBox.Show("Plz Input Digits!!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
                
            
            
        }
        private void CheckAns()
        {
            int your_ans = int.Parse(textBox1.Text);
            if (your_ans == ANS)
            {
                MessageBox.Show("Congratulations!! You r Good");
                Application.Exit();
            }
            else
            {

                if (your_ans > ANS)
                    MAX = your_ans;
                else
                    MIN = your_ans;
                HINT = $"Ans is {MIN}~ {MAX}";
                label1.Text = HINT;
                //MessageBox.Show("Sorry, Try Again~~");
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            if (chkInputIsDigits())
                CheckAns();
            textBox1.Clear();
            //MessageBox.Show("" + ans);
            //else
            //    MessageBox.Show("Plz Input Digits!!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        //MessageBox

    }
}
